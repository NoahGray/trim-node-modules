#!/bin/bash

cd $1
for i in * ; do
  if [ -d "$i" ]; then
    if [ -d "$i/node_modules/" ]; then
      echo "Removing node_modules from $i"
      rm -rf "$i/node_modules";
    fi
  fi
done
