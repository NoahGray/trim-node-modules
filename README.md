# Trim Node Modules

Very simple script that removes `node_modules` folders from all folders, given a parent directory.

```
- MyProjects
  |- my-work-project
    |- node_modules
  |- my-fun-project
    |- node_modules
```

## Usage

Give the script permissions to execute:

```zsh
chmod +x ./trim-node-modules.sh
```

Assuming the folder structure above, run

```zsh
./trim-node-modules.sh ~/MyProjects
```

You should see

```
Removing node_modules my-work-project
Removing node_modules my-fun-project
```

## Caution

I'm not responsible for any damage done to your projects with this. If you don't save your packages to `package.json` and use a lock file, your projects may be hard to recover with a simple `npm install` or `yarn` command.
